/*
  This file is part of CSGS algorithm.

  CSGS algorithm is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  CSGS algorithm is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with CSGS algorithm.  If not, see <http://www.gnu.org/licenses/>.
*/

package csgs;

import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.Collection;
import java.util.Vector;

import csgs.feature.Feature;
import csgs.util.FileHandler;
import csgs.dataset.Dataset;

import csgs.independenceTest.IndependenceTest;
import csgs.independenceTest.ChiSquare;

import csgs.setting.Configuration;
import csgs.setting.FeatureType;

import csgs.adtree.ADNode;
import java.util.List;
import java.util.Map;

public class StructureLearner {

	public static void main(String[] args) {
		long startTime;
		long totalTime;

		if (args.length < 2) {
			System.out.println("Please provide "
					   + "the path to a configuration file, "
					   + "and a dataset name.");
			System.exit(1);
		}

		try {
			Configuration conf = (args.length > 2) ?
				new Configuration(args[0], args[1], args[2]) :
				new Configuration(args[0], args[1], "");

			// Read dataset from file
			Dataset dataset = FileHandler.readDataset(conf.getDatasetFile());

			// Set cardinalities
			int[] card = new int[dataset.getNumberOfVariables()];

			for (int i = 0; i < card.length; i++) card[i] = 2;

			dataset.setCard(card);

			// Make an ADTree for caching sufficient statistics
			// used by independence tests
			if (conf.getCacheForIndependenceTests()) {
				dataset.createCache(conf.getCacheThreshold());
				dataset.setUseCache();
			}

			// Create the independence test from configuration file
			IndependenceTest test;
			if (conf.getIndependenceTestName().contentEquals("chi"))
				test = new ChiSquare(dataset, conf.getIndependenceTestThreshold());
			else
				test = new ChiSquare(dataset, conf.getIndependenceTestThreshold());

			// Generate the set of contexts
			List<Integer> contexts = null;
			if (!conf.getContextsFile().exists()) {
				contexts = new java.util.ArrayList<Integer>();

				for (int i = 0; i < dataset.getList().size(); i++)
					contexts.add(i);
			} else {
				if (!conf.getContextsFile().exists()) {
					System.err.println("file of contexts does not exist.");
					System.exit(1);
				}

				contexts = FileHandler.readContexts(conf.getContextsFile());
			}

			Algorithm csgs = new Algorithm(dataset.getNumberOfVariables(),
						       test,
						       dataset,
						       conf);

			startTime = System.currentTimeMillis();

			Map<Structure, List<Integer>> structures = csgs.execute(contexts);

			totalTime = System.currentTimeMillis() - startTime;

			FileHandler.writeMessage(conf.getLogFile(),
						 "Generated " + structures.keySet().size()
						 + " structures for "
						 + contexts.size()
						 + " contexts in " + totalTime + " ms.");

			// Log number of independence tests performed in Grow Phase
			FileHandler.writeMessage(conf.getLogFile(),
						 "Grow phase - Number of tests:" +
						 csgs.getCountG());
			FileHandler.writeMessage(conf.getLogFile(),
						 "Grow phase - Number of weighted tests:" +
						 csgs.getWcountG());

			// Log number of independence tests performed in Shrink Phase
			FileHandler.writeMessage(conf.getLogFile(),
						 "Shrink phase - Number of tests:" +
						 csgs.getCountS());
			FileHandler.writeMessage(conf.getLogFile(),
						 "Shrink phase - Number of weighted tests:" +
						 csgs.getWcountS());

			// Log number of added and removed edges
			FileHandler.writeMessage(conf.getLogFile(),
						 "Number of added edges:" +
						 csgs.getNumberOfAddedEdges());
			FileHandler.writeMessage(conf.getLogFile(),
						 "Number of removed edges:" +
						 csgs.getNumberOfRemovedEdges());

			// Clean output file where csgs will write on it its the output
			FileHandler.writeNetwork(null, conf.getNetworkFile());

			// Write output structure
			FileHandler.writeNetwork(structures, conf.getNetworkFile());

		} catch (FileNotFoundException e) {
			System.out.println("The configuration file was not found.");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
