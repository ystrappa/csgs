/*
  This file is part of CSGS algorithm.

  CSGS algorithm is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  CSGS algorithm is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with CSGS algorithm.  If not, see <http://www.gnu.org/licenses/>.
*/

package csgs;

import java.util.Collection;
import java.util.BitSet;
import java.util.List;
import java.util.ArrayList;

import csgs.feature.Feature;
import csgs.independenceTest.IndependenceTest;
import csgs.dataset.Dataset;
import csgs.setting.Configuration;
import csgs.setting.FeatureType;
import csgs.util.FileHandler;
import java.util.HashMap;
import java.util.Map;

public class Algorithm {
	protected final IndependenceTest _independenceTest;
	protected final Dataset _dataset;
	protected final int _numberOfVariables;
	protected final Configuration _conf;
	protected long _countG;
	protected long _wcountG;
	protected long _countS;
	protected long _wcountS;
	protected long _numberOfAddedEdges;
	protected long _numberOfRemovedEdges;


	public Algorithm(int numberOfVariables,
			 IndependenceTest independenceTest,
			 Dataset dataset,
			 Configuration conf) {
		this._independenceTest = independenceTest;
		this._dataset = dataset;
		this._numberOfVariables = numberOfVariables;
		this._conf = conf;
		this._countG = 0;
		this._wcountG = 0;
		this._countS = 0;
		this._wcountS = 0;
		this._numberOfAddedEdges = 0;
		this._numberOfRemovedEdges = 0;
	}


	/**
	 *
	 * Since the growth phase can add wrong nodes, the shrink phase
	 * decides which nodes to remove from the neighborhood.
	 *
	 **/
	private Structure
	growthPhase(Structure structure, final Feature context, final int node) {
		// obtain blanket (adjacencies)
		BitSet blanket = (BitSet) structure.getAdjacencies(node).clone();

		// only check nodes that do not belong to blanket
		BitSet remainingNodes = (BitSet) blanket.clone();
		remainingNodes.flip(0, (this._numberOfVariables));

		for (int y = remainingNodes.nextSetBit(node + 1);
		     y >= 0;
		     y = remainingNodes.nextSetBit(y + 1)) {
			List<Integer> contextualVariables = new ArrayList<Integer>();
			List<Integer> contextualValues = new ArrayList<Integer>();

			for (int i = blanket.nextSetBit(0);
			     i >= 0;
			     i = blanket.nextSetBit(i + 1)) {
				contextualVariables.add(i);
				contextualValues.add(context.getVal(i));
			}

			// System.out.println("I: "+x +" " + y + "|" + contextualVariables);

			this._independenceTest.independent(node,
							   y,
							   contextualVariables,
							   contextualValues);
			// Independence tests count
			this._countG++;
			// Weighted independence tests count
			this._wcountG += 2 + contextualVariables.size();

			// add node to blanket if the result of the test is
			// false (truth value = false). The adition of a new
			// node to the blanket implies that there is an edge
			// between node and y
			if (!(this._independenceTest.getTruthValue())) {
				structure.addEdge(node, y);
				blanket.set(y, true);

				this._numberOfAddedEdges++;
			}
		}


		return structure;
	}

	/**
	 *
	 * The shirnk phase decides which nodes to add to the neighborhood. This
	 * phase can add wrong nodes to the neighborhood.
	 *
	 **/
	private Structure
	shrinkPhase(Structure structure, final Feature context, final int node) {
		// obtain blanket (adjacencies)
		BitSet blanket = (BitSet) structure.getAdjacencies(node).clone();

		for (int y = blanket.nextSetBit(node + 1);
		     y >= 0;
		     y = blanket.nextSetBit(y + 1)) {
			blanket.set(y, false);

			List<Integer> contextualVariables = new ArrayList<Integer>();
			List<Integer> contextualValues = new ArrayList<Integer>();

			for (int i = blanket.nextSetBit(0);
			     i >= 0;
			     i = blanket.nextSetBit(i + 1)) {
				contextualVariables.add(i);
				contextualValues.add(context.getVal(i));
			}

			// System.out.println("I: "+node +" " + y + "|" + contextualVariables);

			this._independenceTest.independent(node,
							   y,
							   contextualVariables,
							   contextualValues);

			// Independence tests count
			this._countS++;
			// Weighted independence tests count
			this._wcountS += 2 + contextualVariables.size();

			// Remove node from blanket if the result of the test
			// is true (truth value = true). The removing of a
			// node from the blanket implies that there is non
			// edge between node and y
			if (this._independenceTest.getTruthValue()) {
				structure.rmEdge(node, y);

				this._numberOfRemovedEdges++;
			} else
				blanket.set(y, true);
		}


		return structure;
	}

	/**
	 *
	 * This method search the adjacencies or neighborhood (or blanket) of
	 * each node. This search is realized into two phases: the growth and
	 * shrink phase.
	 *
	 **/
	public Map<Structure, List<Integer>> execute(List<Integer> contexts) {
		Map<Structure, List<Integer>> structures
			= new HashMap<Structure, List<Integer>>();

		List<Integer> contextsOfStructure;
		Structure structure = null;
		BitSet nodesToCheck = new BitSet(this._numberOfVariables);
		nodesToCheck.set(0, this._numberOfVariables);

		int i = 0;
		Feature context = null;
		for (Integer idx_context : contexts) {
			long startTime = System.currentTimeMillis();

			// retrieve context from the dataset using the id
			context = this._dataset.getList().get(idx_context);

			String strOutput = "(" + (++i) + "/"
				+ contexts.size()
				+ ") " + context;

			// initial structure
			structure = new Structure(this._numberOfVariables);

			// search the neighborhood of each node detailed by
			// the bitstring nodeToCheck
			for (int nodeToCheck = nodesToCheck.nextSetBit(0);
			     nodeToCheck >= 0;
			     nodeToCheck = nodesToCheck.nextSetBit(nodeToCheck + 1)) {
				structure = growthPhase(structure, context, nodeToCheck);
				structure = shrinkPhase(structure, context, nodeToCheck);
			}

			// Does this new structure exist? If it doesn't then create an entry
			contextsOfStructure = structures.containsKey(structure) ?
				structures.get(structure) : new ArrayList<Integer>();

			contextsOfStructure.add(idx_context);
			structures.put(structure, contextsOfStructure);

			long totalTime = System.currentTimeMillis() - startTime;
			FileHandler.writeMessage(_conf.getLogFile(),
						 strOutput + " in " + totalTime + " ms.");
		}

		return structures;
	}

	public long getCountG() {
		return this._countG;
	}

	public long getWcountG() {
		return this._wcountG;
	}

	public long getCountS() {
		return this._countS;
	}

	public long getWcountS() {
		return this._wcountS;
	}

	public long getNumberOfAddedEdges() {
		return this._numberOfAddedEdges;
	}

	public long getNumberOfRemovedEdges() {
		return this._numberOfRemovedEdges;
	}
}
