/*
  This file is part of CSGS algorithm.

  CSGS algorithm is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  CSGS algorithm is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with CSGS algorithm.  If not, see <http://www.gnu.org/licenses/>.
*/

package csgs.setting;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Configuration {
	private final String _datasetName;
	private final String _datasetDirectory;
	private final String _networkDirectory;
	private final String _logDirectory;
	private final String _independenceTestName;
	private final double _independenceTestThreshold;
	private final FeatureType _featureType;
	private final boolean _cacheForIndependenceTests;
	private final int _cacheThreshold;
	private final String _contextsFile;

	// Read the configuration
	public
	Configuration(String pathToConfigurationFile,
		      String datasetName,
		      String contextsFile)
		throws FileNotFoundException, IOException {
		this._datasetName = datasetName;
		Scanner scanner = new Scanner(new File(pathToConfigurationFile));
		this._datasetDirectory = scanner.nextLine();
		this._networkDirectory = scanner.nextLine();
		this._logDirectory = scanner.nextLine();

		// Set type of feature
		String strFeatureType = scanner.nextLine();

		if (strFeatureType.equalsIgnoreCase("positive")) {
			this._featureType = FeatureType.POSITIVE;
		} else if (strFeatureType.equalsIgnoreCase("mixed")) {
			this._featureType = FeatureType.MIXED;
		} else {
			this._featureType = FeatureType.BINARY;
		}

		this._independenceTestName = scanner.nextLine();
		this._independenceTestThreshold = Double.parseDouble(scanner.nextLine());
		this._cacheForIndependenceTests = Boolean.parseBoolean(scanner.nextLine());
		this._cacheThreshold = Integer.parseInt(scanner.nextLine());
		this._contextsFile = contextsFile;
	}

	public Configuration(String datasetName,
			     String datasetDirectory,
			     String networkDirectory,
			     String logDirectory,
			     String cacheDirectory,
			     String independenceTestName,
			     double thresholdIndependenceTest,
			     FeatureType featureType,
			     boolean cacheForIndependenceTests,
			     int cacheThreshold,
			     String strategy,
			     double gsThreshold,
			     String contextsFile) {
		this._datasetName = datasetName;
		this._datasetDirectory = datasetDirectory;
		this._networkDirectory = networkDirectory;
		this._logDirectory = logDirectory;
		this._independenceTestName = independenceTestName;
		this._independenceTestThreshold = thresholdIndependenceTest;
		this._featureType = featureType;
		this._cacheForIndependenceTests = cacheForIndependenceTests;
		this._cacheThreshold = cacheThreshold;
		this._contextsFile = contextsFile;
	}

	public File getDatasetFile() {
		return new File(this.getDatasetDirectory()
				+ File.separator
				+ this.getDatasetName());
	}

	public String getBaseFileName() {

		String baseFileName = "";

		if (this._contextsFile.equals("")) {

			baseFileName += this.getDatasetName()
				+ "_test" + this.getIndependenceTestName()
				+ "_thr" + this.getIndependenceTestThreshold()
				+ "_" + this.getFeatureType();
		} else {
			baseFileName += this.getDatasetName()
				+ "_" + this.getContextsFile().getName()
				+ "_test" + this.getIndependenceTestName()
				+ "_thr" + this.getIndependenceTestThreshold()
				+ "_" + this.getFeatureType();
		}

		return baseFileName;
	}

	public File getNetworkFile() {
		return new File(this.getNetworkDirectory()
				+ File.separator
				+ getBaseFileName() + ".net");
	}

	public File getFeatureFile() {
		return new File(this.getNetworkDirectory()
				+ File.separator
				+ this.getDatasetName()
				+ "_test" + this.getIndependenceTestName()
				+ "_thr" + this.getIndependenceTestThreshold()
				+ ".features");
	}

	public File getLogFile() {
		return new File(getLogDirectory()
				+ File.separator
				+ this.getBaseFileName() + ".log");
	}

	public double getIndependenceTestThreshold() {
		return this._independenceTestThreshold;
	}

	public String getIndependenceTestName() {
		return this._independenceTestName;
	}

	private String getDatasetName() {
		return this._datasetName;
	}

	private String getDatasetDirectory() {
		return this._datasetDirectory;
	}

	private String getNetworkDirectory() {
		return this._networkDirectory;
	}

	private String getLogDirectory() {
		return this._logDirectory;
	}

	public FeatureType getFeatureType() {
		return this._featureType;
	}

	public boolean getCacheForIndependenceTests() {
		return this._cacheForIndependenceTests;
	}

	public int getCacheThreshold() {
		return this._cacheThreshold;
	}

	public File getContextsFile() {
		return new File(this._contextsFile);
	}
}
