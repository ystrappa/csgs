/*
  This file is part of CSGS algorithm.

  CSGS algorithm is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  CSGS algorithm is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with CSGS algorithm.  If not, see <http://www.gnu.org/licenses/>.
*/

package csgs.util;


import csgs.feature.BinaryFeature;
import csgs.feature.Feature;
import csgs.dataset.Dataset;
import csgs.Structure;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import java.text.SimpleDateFormat;


public class FileHandler {
	private static void
	readWDataDataset(File inputFile, Dataset dataset) throws IOException {
		// Open the file reader
		FileReader fileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		int numberOfVariables = 0;
		String line = null;

		while ((line = bufferedReader.readLine()) != null) {
			String[] parts = line.trim().split("\\|");
			String[] items = parts[1].split(",");

			numberOfVariables = items.length;
			Feature example = new BinaryFeature(numberOfVariables);

			for (int i = 0; i < numberOfVariables; i++) {
				example.addVal(i, Integer.parseInt(items[i]));
			}

			for (int i = 0; i < Integer.parseInt(parts[0]); i++) {
				dataset.addExample(example);
			}
		}

		bufferedReader.close();

		dataset.setNumberOfVariables(numberOfVariables);
	}

	public static Dataset
	readDataset(File inputFile) {
		Dataset dataset = new Dataset();

		try {
			readWDataDataset(inputFile, dataset);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		return dataset;
	}


	public static void writeMessage(File logFile, String message) {
		try {
			// Open the writer
			FileWriter fileWriter = new FileWriter(logFile, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// Write the message
			bufferedWriter.write(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").format(new Date()) + "\t");
			bufferedWriter.write(message + "\n");

			// Close the writer
			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static List<Integer> readContexts(File inputFile)
		throws IOException {
		// Open the file reader
		FileReader fileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		List<Integer> contexts = new java.util.ArrayList<Integer>();

		int numberOfVariables = -1;
		String line = null;
		while ((line = bufferedReader.readLine()) != null)
			contexts.add(Integer.parseInt(line.trim()));

		bufferedReader.close();

		return contexts;
	}


	public static void
	writeNetwork(Map<Structure, List<Integer>> structure,  File networkFile) {
		try {
			FileWriter fileWriter;

			// Clean networkFile
			if (structure == null) {
				fileWriter = new FileWriter(networkFile);
				return;
			}

			fileWriter = new FileWriter(networkFile, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			int i = 0;
			for (Map.Entry<Structure, List<Integer>> s : structure.entrySet()) {
				// Write the network
				bufferedWriter.write(s.getKey().toString());
				bufferedWriter.write(";");

				for (i = 0; i < s.getValue().size() - 1; i++)
					bufferedWriter.write(s.getValue().get(i) + " ");

				bufferedWriter.write(s.getValue().get(i)+ "\n");
			}

			// Close the writer
			bufferedWriter.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}
