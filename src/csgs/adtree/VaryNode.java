/*
 * VaryNode.java
 * Copyright (C) 2002-2012 University of Waikato, Hamilton, New Zealand
 *
 */
/*
  This file is part of CSGS algorithm.

  CSGS algorithm is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  CSGS algorithm is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with CSGS algorithm.  If not, see <http://www.gnu.org/licenses/>.
*/


package csgs.adtree;

import csgs.dataset.Dataset;

/**
 * Part of ADTree implementation. See ADNode.java for more details.
 *
 */

public class VaryNode implements java.io.Serializable {
  /** index of the node varied **/
  public int m_iNode;

  /** most common value **/
  public int m_nMCV;

  /** list of ADNode children **/
  public ADNode [] m_ADNodes;

  /** Creates new VaryNode */
  public VaryNode(int iNode) {
    m_iNode = iNode;
  }

  /**
   * print is used for debugging only, called from ADNode
   *
   * @param sTab amount of space.
   */
  public void print(String sTab) {
    for (int iValue = 0; iValue < m_ADNodes.length; iValue++) {
      System.out.print(sTab + iValue + ": ");
      if (m_ADNodes[iValue] == null) {
	if (iValue == m_nMCV) {
	  System.out.println("MCV");
	} else {
	  System.out.println("null");
	}
      } else {
	System.out.println();
	m_ADNodes[iValue].print();
      }
    }
  }
}
