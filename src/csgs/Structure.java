/*
  This file is part of CSGS algorithm.

  CSGS algorithm is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  CSGS algorithm is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with CSGS algorithm.  If not, see <http://www.gnu.org/licenses/>.
*/

package csgs;

import java.util.BitSet;
import java.util.Arrays;


public class Structure {
	private final BitSet[] _adjacencies;

	public Structure(int numberOfVariables) {
		this._adjacencies = new BitSet[numberOfVariables];
                for (int i = 0; i < numberOfVariables; i++)
                    this._adjacencies[i] = new BitSet(numberOfVariables);
	}

	public Structure(Structure structure) {
		this._adjacencies = new BitSet[structure.getNumberOfVariables()];

		for (int i = 0; i < structure.getNumberOfVariables(); i++)
			this._adjacencies[i] = (BitSet) structure.getAdjacencies(i).clone();
	}

	public BitSet getAdjacencies(int variable) {
		return this._adjacencies[variable];
	}

	public void addEdge(int x, int y) {
		if (this._adjacencies[x] == null)
			this._adjacencies[x] = new BitSet(this.getNumberOfVariables());

		if (this._adjacencies[y] == null)
			this._adjacencies[y] = new BitSet(this.getNumberOfVariables());

		this._adjacencies[x].set(y, true);
		this._adjacencies[y].set(x, true);
	}

	public void rmEdge(int x, int y) {
		this._adjacencies[x].set(y, false);
		this._adjacencies[y].set(x, false);
	}

	// This method removes all the edges of the form (x, y), where y \in
	// \mb(x). This results in removing the nodes in the Markov blanket of
	// x
	public void clearMarkovBlanket(int x) {
		for (int y = this._adjacencies[x].nextSetBit(0);
		     y >= 0;
		     y = this._adjacencies[x].nextSetBit(y + 1))
			this.rmEdge(x, y);
	}

	public int getNumberOfVariables() {
		return _adjacencies.length;
	}

	public Structure clone() {
		return new Structure(this);
	}

	public boolean equals(Object object) {
		if (object instanceof Structure) {
			Structure st = (Structure) object;

			return Arrays.deepEquals(this._adjacencies, st._adjacencies);
		}
		else
			return false;
	}

	public int hashCode() {
		return Arrays.deepHashCode(this._adjacencies.clone());
	}

	// the space complexity of the output string is \choose{n}{2} in the
	// worst case (fully connected graph)
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		int j;
		// only print the adjacencies of n - 1 variables, the last one
		// is not necessary (they can be inferred from the remaining
		// variables)
		for (int i = 0; i < this.getNumberOfVariables() - 1; i++) {
			for (j = i + 1; j < this.getNumberOfVariables(); j++)
				if (this._adjacencies[i].get(j))
					stringBuilder.append(j + " ");

			if (this._adjacencies[i].get(j))
				stringBuilder.append(j);

			stringBuilder.append(",");
		}

		return stringBuilder.toString().replaceAll("\\s,", ",").replaceAll(",$","");
	}
}
